<?php

namespace Drupal\feeds_instagram\Feeds\Fetcher;

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\ClearableInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\RawFetcherResult;
use Drupal\feeds\StateInterface;
use Drupal\Component\Utility\Unicode;

/**
 * Constructs FeedsInstagramFetcher object.
 *
 * @FeedsFetcher(
 *   id = "feeds_instagram_fetcher",
 *   title = @Translation("Instagram"),
 *   description = @Translation("Fetch media from Instagram"),
 *   form = {
 *     "configuration" = "Drupal\feeds_instagram\Feeds\Fetcher\Form\FeedsInstagramFetcherForm",
 *     "feed" = "Drupal\feeds_instagram\Feeds\Fetcher\Form\FeedsInstagramFetcherFeedForm",
 *   }
 * )
 */
class FeedsInstagramFetcher extends PluginBase implements ClearableInterface, FetcherInterface {

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $feed_config = $feed->getConfigurationFor($this);
    $result = $this->get($feed->id(), $feed_config['instagram_business_account_id']);
    if ($result !== FALSE) {
      return new RawFetcherResult($result);
    }
    else {
      return new RawFetcherResult('');
    }
  }

  /**
   * Helper function to get client factory.
   *
   * @param string $feed_id
   *   The feed Id.
   *
   * @return \Facebook\Facebook|null
   *   Returns the Facebook client.
   */
  public function getClientFactory($feed_id) {
    $config = $this->getConfiguration();
    $cid = $this->getAccessTokenCacheId($feed_id);
    $facebook_access_token = \Drupal::service('cache.feeds_instagram_tokens')->get($cid);

    if (!empty($config['facebook_app_id']) && !empty($config['facebook_app_secret'])) {
      $params = [
        'app_id' => $config['facebook_app_id'],
        'app_secret' => $config['facebook_app_secret'],
        'default_graph_version' => 'v9.0',
      ];

      // Set default access token if one exists.
      if (!empty($facebook_access_token->data)) {
        $params['default_access_token'] = $facebook_access_token->data;
      }

      // Create Facebook client.
      return new Facebook($params);
    }
    else {
      \Drupal::messenger()->addWarning($this->t('Facebook Open Graph access is not configured.'));
    }
  }

  /**
   * Make the API queries to get the data the parser needs.
   *
   * @param string $feed_id
   *   The feed Id.
   * @param string $instagram_business_account_id
   *   The Instagram ID to use.
   *
   * @return string
   *   Returns a JSON-encoded array of stdClass objects.
   */
  public function get($feed_id, $instagram_business_account_id) {
    $client = $this->getClientFactory($feed_id);

    $import_limit = $this->getConfiguration('import_limit');
    if ($import_limit < 1) {
      $import_limit = 1;
    }

    $result = [];
    $response = $client->get($instagram_business_account_id . '/media');

    $media_edge = $response->getGraphEdge();

    if (count($media_edge) > 0) {
      do {
        foreach ($media_edge as $media) {
          $result[] = $this->parseMediaItem($client, $media->getField('id'));
          if (count($result) >= $import_limit) {
            break;
          }
        }
      } while (count($result) < $import_limit && $media_edge = $client->next($media_edge));
    }
    else {
      \Drupal::messenger()->addStatus($this->t('No media items found.'));
    }

    return json_encode($result);
  }

  /**
   * Make a Facebook Open Graph API request.
   *
   * @param \Facebook\Facebook $client
   *   The Facebook client to use.
   * @param string $request
   *   The request to make.
   *
   * @return \Facebook\FacebookResponse
   *   Returns the response.
   */
  public function getClientResponse(Facebook $client, $request) {
    try {
      $response = $client->get($request);
    }
    catch (FacebookResponseException $e) {
      // When Graph returns an error.
      \Drupal::messenger()->addWarning($this->t('Facebook Open Graph Error: @error', ['@error' => $e->getMessage()]));
    }
    catch (FacebookSDKException $e) {
      // When validation fails or other local issues.
      \Drupal::messenger()->addWarning($this->t('Facebook SDK Error: @error', ['@error' => $e->getMessage()]));
    }

    return $response;
  }

  /**
   * Parse an Instagram media item.
   *
   * @param \Facebook\Facebook $client
   *   The Facebook client to use.
   * @param string $media_id
   *   The Instagram media item ID.
   *
   * @return array
   *   Returns the parsed media item.
   */
  private function parseMediaItem(Facebook $client, $media_id) {
    $response = $this->getClientResponse($client, $media_id . '?fields=id,ig_id,shortcode,timestamp,media_type,media_url,thumbnail_url,caption,permalink,like_count,comments_count,owner,username,is_comment_enabled,children,comments');

    $body = $response->getDecodedBody();
    return [
      'id' => $body['id'],
      'instagram_id' => $body['ig_id'],
      'shortcode' => $body['shortcode'],
      'published_datetime' => date('Y-m-d H:i:s', strtotime($body['timestamp'])),
      'published_timestamp' => strtotime($body['timestamp']),
      'type' => $body['media_type'],
      'media_url' => isset($body['media_url']) ? $body['media_url'] : '',
      'thumbnail_url' => isset($body['thumbnail_url']) ? $body['thumbnail_url'] : '',
      'caption' => $body['caption'],
      'title' => !empty($body['caption']) ? Unicode::truncate($body['caption'], 255, FALSE, TRUE) : $this->t('Instagram post by @username (@ig_id)', ['@username' => $body['username'], '@ig_id' => $body['ig_id']]),
      'permalink' => $body['permalink'],
      'like_count' => $body['like_count'],
      'comments_count' => $body['comments_count'],
      'owner_id' => isset($body['owner']) && isset($body['owner']['id']) ? $body['owner']['id'] : '',
      'username' => $body['username'],
      'is_comment_enabled' => $body['is_comment_enabled'] ? 1 : 0,
      'children' => isset($body['children']) ? $body['children'] : [],
      'comments' => isset($body['comments']) ? $body['comments'] : [],
    ];
  }

  /**
   * Get access token cache ID.
   *
   * @param string $feed_id
   *   The feed Id.
   *
   * @return string
   *   Returns the cache ID.
   */
  public function getAccessTokenCacheId($feed_id) {
    return 'feeds_instagram:facebook_access_token:' . $feed_id;
  }

  /**
   * {@inheritdoc}
   */
  public function clear(FeedInterface $feed, StateInterface $state) {
    $this->onFeedDeleteMultiple([$feed]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration() {
    return [
      'facebook_page_id' => '',
      'instagram_business_account_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'facebook_app_id' => '',
      'facebook_app_secret' => '',
      'import_limit' => 50,
    ];
  }

}
