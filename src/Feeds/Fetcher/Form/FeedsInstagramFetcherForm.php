<?php

namespace Drupal\feeds_instagram\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

/**
 * The configuration form for Instagram fetchers.
 */
class FeedsInstagramFetcherForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->plugin->getConfiguration();

    $form['facebook_app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#description' => $this->t('Facebook App ID'),
      '#default_value' => $config['facebook_app_id'],
      '#required' => TRUE,
    ];
    $form['facebook_app_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Secret'),
      '#description' => $this->t('Facebook App Secret'),
      '#default_value' => $config['facebook_app_secret'],
      '#required' => TRUE,
    ];
    $form['import_limit'] = [
      '#type' => 'number',
      '#min' => 1,
      '#title' => $this->t('Limit the total number of imported media items'),
      '#description' => $this->t('Specify a limit for the total number of media items to import from Instagram.'),
      '#default_value' => $config['import_limit'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Trim all values before saving.
    $values = $form_state->getValues();
    foreach ($values as &$value) {
      if (is_string($value)) {
        $value = trim($value);
      }
    }
    $this->plugin->setConfiguration($values);
  }

}
