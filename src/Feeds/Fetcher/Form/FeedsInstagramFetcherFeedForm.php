<?php

namespace Drupal\feeds_instagram\Feeds\Fetcher\Form;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a form on the feed edit page for the FeedsInstagramFetcher.
 */
class FeedsInstagramFetcherFeedForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    $feed_config = $feed->getConfigurationFor($this->plugin);

    // Authenticate/revoke access to Facebook Account.
    $config = $this->plugin->getConfiguration();
    if ($feed->id() && !empty($config['facebook_app_id']) && !empty($config['facebook_app_secret'])) {
      $client = $this->plugin->getClientFactory($feed->id());
      $helper = $client->getRedirectLoginHelper();

      // Get existing token (if one exists) and check for token code parameter.
      $cid = $this->plugin->getAccessTokenCacheId($feed->id());
      $facebook_access_token = \Drupal::service('cache.feeds_instagram_tokens')->get($cid);
      $code = \Drupal::request()->query->get('code');

      $form['facebook_access'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Facebook OAuth 2.0 Authentication'),
      ];

      if (!empty($code)) {
        try {
          $facebook_access_token = $helper->getAccessToken();
        }
        catch (FacebookResponseException $e) {
          // When Graph returns an error.
          \Drupal::messenger()->addWarning($this->t('Facebook Open Graph Error: @error', [
            '@error' => $e->getMessage(),
          ]));
        }
        catch (FacebookSDKException $e) {
          // When validation fails or other local issues.
          \Drupal::messenger()->addWarning($this->t('Facebook SDK Error: @error', [
            '@error' => $e->getMessage(),
          ]));
        }

        // Validate access token.
        if (!isset($facebook_access_token)) {
          if ($helper->getError()) {
            \Drupal::messenger()->addWarning($this->t('Facebook Authorization Error: @error', [
              '@error' => $helper->getError(),
            ]));
          }
          else {
            \Drupal::messenger()->addWarning($this->t('Facebook Authorization Bad Request'));
          }
        }
        elseif (!$facebook_access_token->isLongLived()) {
          // Get the OAuth 2.0 client handler for managing access tokens.
          $oauth2_client = $client->getOAuth2Client();

          // Exchanges a short-lived access token for a long-lived one.
          try {
            $facebook_access_token = $oauth2_client->getLongLivedAccessToken($facebook_access_token);
          }
          catch (FacebookSDKException $e) {
            \Drupal::messenger()->addWarning($this->t('Facebook Access Token Exchange Error: @error', [
              '@error' => $e->getMessage(),
            ]));
          }
        }

        // Save access token.
        $cache_tags = [
          'feeds_instagram:facebook_access_token',
          $cid,
        ];
        \Drupal::service('cache.feeds_instagram_tokens')->set($cid, $facebook_access_token, Cache::PERMANENT, $cache_tags);

        // Let the user know they successfully authenticated.
        \Drupal::messenger()->addStatus($this->t('You have successfully authenticated your website to use the Facebook Open Graph API. Please select the Facebook Page connected to the Instagram account that you would like to use.'));

        // Redirect back to the original page.
        $current_path = \Drupal::service('path.current')->getPath();
        $response = new RedirectResponse($current_path, 302);
        $response->send();
      }
      elseif (empty($facebook_access_token->data)) {
        $current_path = \Drupal::service('path.current')->getPath();
        $path_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
        $redirect_uri = Url::fromUserInput($path_alias, ['absolute' => TRUE])->toString();

        $form['facebook_access']['info'] = [
          '#type' => 'markup',
          '#markup' => '<p><em>' . $this->t('Before authenticating, you must add <a href=":link">this URL</a> as an authorized redirect URI to your Facebook App.', [
            ':link' => $redirect_uri,
          ]) . '</em></p>',
        ];

        // Create login link.
        $permissions = [
          'pages_read_engagement',
          'pages_show_list',
          'instagram_basic',
        ];
        $url = Url::fromUri($helper->getLoginUrl($redirect_uri, $permissions));
        $auth_link = Link::fromTextAndUrl($this->t('Authenticate Facebook Account'), $url);
        $auth_link = $auth_link->toRenderable();
        $auth_link['#attributes'] = ['class' => ['button', 'button--primary']];
        $form['facebook_access']['grant_access'] = [
          '#type' => 'markup',
          '#markup' => '<p>' . \Drupal::service('renderer')->render($auth_link) . '</p>',
        ];
      }
      else {
        if (!empty($facebook_access_token->data->getExpiresAt())) {
          $date = $facebook_access_token->data->getExpiresAt()->getTimestamp();
          $form['facebook_access']['info'] = [
            '#type' => 'markup',
            '#markup' => '<p><em>' . $this->t('Expires: @date', [
              '@date' => \Drupal::service('date.formatter')->format($date, 'long'),
            ]) . '</em></p>',
          ];
        }

        $form['facebook_access']['revoke_access'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Revoke access to Facebook Account'),
          '#default_value' => FALSE,
        ];

        // Get user pages.
        $response = FALSE;
        try {
          $response = $client->get('me/accounts?fields=id,name');
        }
        catch (FacebookResponseException $e) {
          // When Graph returns an error.
          \Drupal::messenger()->addWarning($this->t('Facebook Open Graph Error: @error', [
            '@error' => $e->getMessage(),
          ]));
        }

        if ($response) {
          $accounts_edge = $response->getGraphEdge();
          if (count($accounts_edge) > 0) {
            $options = [];
            foreach ($accounts_edge as $page) {
              $options[$page->getField('id')] = $page->getField('name');
            }

            $form['facebook_access']['facebook_page_id'] = [
              '#type' => 'select',
              '#title' => $this->t('Facebook Page'),
              '#description' => $this->t('Select which Facebook Page has the intended Instagram Business Account to use.'),
              '#options' => $options,
              '#default_value' => $feed_config['facebook_page_id'],
            ];
          }
          else {
            \Drupal::messenger()->addWarning($this->t('The authenticated user does not have any pages.'));
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    // Revoke access token.
    if ($form_state->getValue(['facebook_access', 'revoke_access'])) {
      // Clear cached token.
      Cache::invalidateTags([$this->plugin->getAccessTokenCacheId($feed->id())]);

      // Let user know the access token has been revoked.
      \Drupal::messenger()->addStatus($this->t('Facebook Open Graph access token revoked.'));
    }
    // Set Instagram Business account ID.
    elseif ($page_id = $form_state->getValue(['facebook_access', 'facebook_page_id'])) {
      $client = $this->plugin->getClientFactory($feed->id());
      $response = $this->plugin->getClientResponse($client, $page_id . '?fields=instagram_business_account');

      $body = $response->getDecodedBody();
      $feed->setConfigurationFor($this->plugin, [
        'facebook_page_id' => $body['id'],
        'instagram_business_account_id' => $body['instagram_business_account']['id'],
      ]);
    }
  }

}
